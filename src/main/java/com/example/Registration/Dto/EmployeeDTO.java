package com.example.Registration.Dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.internal.build.AllowPrintStacktrace;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {

//utilizzare lombok
    private int epmloyeeid;
    private String employeename;
    private String address;
    private int mobile;
}
